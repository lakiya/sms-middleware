/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.manager;

import com.dms.entity.SmsTransaction;
import com.dms.entity.SmsTransactionDetail;
import org.springframework.stereotype.Component;

/**
 *
 * @author Lakshitha
 */
@Component
public interface CoreBankSmsStatusWriter {
    Boolean updateCoreBankTransactionDetailStatus(SmsTransactionDetail smsTransactionDetail);
}
