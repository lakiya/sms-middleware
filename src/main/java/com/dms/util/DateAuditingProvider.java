/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.util;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Optional;
import org.springframework.data.auditing.DateTimeProvider;

/**
 *
 * @author Lakshitha
 */
public class DateAuditingProvider implements DateTimeProvider {

    @Override
    public Calendar getNow() {
        return Calendar.getInstance();
    }  
}