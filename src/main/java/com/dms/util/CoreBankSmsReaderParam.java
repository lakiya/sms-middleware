/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.util;

/**
 *
 * @author Lakshitha
 */
public class CoreBankSmsReaderParam {
    public final static Integer MASKED_ACCOUNT_NO = 0;
    public final static Integer CUSTOMER_NO = 1;
    public final static Integer MOBILE_NO = 2;
    public final static Integer AMOUNT = 3;
    public final static Integer TXN_CATEGORY = 4;
    public final static Integer REF_NO = 5;
    public final static Integer CHEQUE_NO = 6;
    public final static Integer SECONDARY_ACC_NO = 7;
    public final static Integer DUE_DATE = 8;
    public final static Integer TXN_TRACE_NO = 9;
    public final static Integer TXN_DATE = 10;
    public final static Integer STATUS = 11;
    public final static Integer EXCEPTION = 12;     
}
