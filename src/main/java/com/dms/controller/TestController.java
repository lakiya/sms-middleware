/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.controller;

import com.dms.manager.CoreBankSmsReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lakshitha
 */
@Controller
@RequestMapping("test")
public class TestController {

    @Autowired
    private CoreBankSmsReader coreBankSmsReader;

    @RequestMapping()
    public void sampleMethod() {
        String strTxnDateTime = "2017-12-20-04.25.30";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        try {
            Date txnDateTime = sdf.parse(strTxnDateTime);
            System.out.println(txnDateTime);
//        coreBankSmsReader.findNextPendingTransaction();
        } catch (ParseException ex) {
            Logger.getLogger(TestController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
