/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.service;

import com.dms.entity.SmsTransactionDetail;
import com.dms.entity.TransactionDetailPK;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Lakshitha
 */
@Repository
public interface SmsTransactionDetailsService extends JpaRepository<SmsTransactionDetail, TransactionDetailPK>{
    List<SmsTransactionDetail> findBySmsStatusNotAndCbsStatus(Integer smsStatus, Integer cbsStatus);
}
