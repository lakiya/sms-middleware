/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lakshitha
 */
@Embeddable
public class TransactionDetailPK implements Serializable {

    private Integer transactionId;
    private String mobileNo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "TRANSACTION_ID")
    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 65)
    @Column(name = "MOBILE_NO")
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        hash += (mobileNo != null ? mobileNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionDetailPK)) {
            return false;
        }
        TransactionDetailPK other = (TransactionDetailPK) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId))) {
            return false;
        }
        if ((this.mobileNo == null && other.mobileNo != null) || (this.mobileNo != null && !this.mobileNo.equals(other.mobileNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dms.entity.TransactionDetailPK[ transactionId=" + transactionId + ", mobileNo=" + mobileNo + " ]";
    }

}
