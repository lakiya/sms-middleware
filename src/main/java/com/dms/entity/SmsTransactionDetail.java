/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Lakshitha
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "SMS_TRANSACTION_DETAIL")
@IdClass(TransactionDetailPK.class)
public class SmsTransactionDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer transactionId;
    private String mobileNo;
    private Integer smsStatus;
    private Integer cbsStatus;
    private String entUser;
    private Date entDateTime;
    private String entPc;
    private String modUser;
    private Date modDateTime;
    private String modPc;

    @Id
    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    @Id
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "SMS_STATUS")
    public Integer getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(Integer smsStatus) {
        this.smsStatus = smsStatus;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "CBS_STATUS")
    public Integer getCbsStatus() {
        return cbsStatus;
    }

    public void setCbsStatus(Integer cbsStatus) {
        this.cbsStatus = cbsStatus;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ENT_USER")
    @CreatedBy
    public String getEntUser() {
        return entUser;
    }

    public void setEntUser(String entUser) {
        this.entUser = entUser;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "ENT_DATE_TIME")
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEntDateTime() {
        return entDateTime;
    }

    public void setEntDateTime(Date entDateTime) {
        this.entDateTime = entDateTime;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ENT_PC")
    public String getEntPc() {
        return entPc;
    }

    public void setEntPc(String entPc) {
        this.entPc = entPc;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "MOD_USER")
    @LastModifiedBy
    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "MOD_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    public Date getModDateTime() {
        return modDateTime;
    }

    public void setModDateTime(Date modDateTime) {
        this.modDateTime = modDateTime;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "MOD_PC")
    public String getModPc() {
        return modPc;
    }

    public void setModPc(String modPc) {
        this.modPc = modPc;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.transactionId);
        hash = 67 * hash + Objects.hashCode(this.mobileNo);
        hash = 67 * hash + Objects.hashCode(this.smsStatus);
        hash = 67 * hash + Objects.hashCode(this.cbsStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SmsTransactionDetail other = (SmsTransactionDetail) obj;
        if (!Objects.equals(this.transactionId, other.transactionId)) {
            return false;
        }
        if (!Objects.equals(this.mobileNo, other.mobileNo)) {
            return false;
        }
        if (!Objects.equals(this.smsStatus, other.smsStatus)) {
            return false;
        }
        if (!Objects.equals(this.cbsStatus, other.cbsStatus)) {
            return false;
        }
        return true;
    }

}
