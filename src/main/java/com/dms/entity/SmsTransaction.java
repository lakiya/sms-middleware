/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Lakshitha
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "SMS_TRANSACTION", schema = "dbo")
public class SmsTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer transactionId;
    private String maskedAccountNo;
    private String customerNo;
    private Double amount;
    private String transactionCategory;
    private String referenceNo;
    private String chequeNo;
    private String secondaryAccountNo;
    private String dueDate;
    private int txnTraceNo;
    private Date txnDatetime;
    private String status;
    private String exception;
    private String reason;
    private String entUser;
    private Date entDateTime;
    private String entPc;
    private String modUser;
    private Date modDateTime;
    private String modPc;

    private List<SmsTransactionDetail> transactionDetailList;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TRANSACTION_ID")
    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "MASKED_ACCOUNT_NO")
    public String getMaskedAccountNo() {
        return maskedAccountNo;
    }

    public void setMaskedAccountNo(String maskedAccountNo) {
        this.maskedAccountNo = maskedAccountNo;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "CUSTOMER_NO")
    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "AMOUNT")
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "TRANSACTION_CATEGORY")
    public String getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(String transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "REFERENCE_NO")
    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "CHEQUE_NO")
    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SECONDARY_ACCOUNT_NO")
    public String getSecondaryAccountNo() {
        return secondaryAccountNo;
    }

    public void setSecondaryAccountNo(String secondaryAccountNo) {
        this.secondaryAccountNo = secondaryAccountNo;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "DUE_DATE")
    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "TXN_TRACE_NO")
    public int getTxnTraceNo() {
        return txnTraceNo;
    }

    public void setTxnTraceNo(int txnTraceNo) {
        this.txnTraceNo = txnTraceNo;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "TXN_DATETIME")
    public Date getTxnDatetime() {
        return txnDatetime;
    }

    public void setTxnDatetime(Date txnDatetime) {
        this.txnDatetime = txnDatetime;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Size(max = 1)
    @Column(name = "EXCEPTION")
    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    @Size(max = 65)
    @Column(name = "REASON")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ENT_USER")
    @CreatedBy
    public String getEntUser() {
        return entUser;
    }

    public void setEntUser(String entUser) {
        this.entUser = entUser;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "ENT_DATE_TIME")
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEntDateTime() {
        return entDateTime;
    }

    public void setEntDateTime(Date entDateTime) {
        this.entDateTime = entDateTime;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ENT_PC")
    public String getEntPc() {
        return entPc;
    }

    public void setEntPc(String entPc) {
        this.entPc = entPc;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "MOD_USER")
    @LastModifiedBy
    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "MOD_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    public Date getModDateTime() {
        return modDateTime;
    }

    public void setModDateTime(Date modDateTime) {
        this.modDateTime = modDateTime;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "MOD_PC")
    public String getModPc() {
        return modPc;
    }

    public void setModPc(String modPc) {
        this.modPc = modPc;
    }

    @Transient
    public List<SmsTransactionDetail> getTransactionDetailList() {
        return transactionDetailList;
    }

    public void setTransactionDetailList(List<SmsTransactionDetail> transactionDetailList) {
        this.transactionDetailList = transactionDetailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmsTransaction)) {
            return false;
        }
        SmsTransaction other = (SmsTransaction) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dms.entity.Transaction[ transactionId=" + transactionId + " ]";
    }
}
