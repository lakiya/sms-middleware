/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.managerImpl;

import com.dms.entity.SmsTransaction;
import com.dms.entity.SmsTransactionDetail;
import com.dms.entity.TransactionDetailPK;
import com.dms.enums.SMSCoreStatus;
import com.dms.enums.SMSStatus;
import com.dms.manager.CoreBankSmsReader;
import static com.dms.util.CoreBankSmsReaderParam.AMOUNT;
import static com.dms.util.CoreBankSmsReaderParam.CHEQUE_NO;
import static com.dms.util.CoreBankSmsReaderParam.CUSTOMER_NO;
import static com.dms.util.CoreBankSmsReaderParam.DUE_DATE;
import static com.dms.util.CoreBankSmsReaderParam.EXCEPTION;
import static com.dms.util.CoreBankSmsReaderParam.MASKED_ACCOUNT_NO;
import static com.dms.util.CoreBankSmsReaderParam.MOBILE_NO;
import static com.dms.util.CoreBankSmsReaderParam.REF_NO;
import static com.dms.util.CoreBankSmsReaderParam.SECONDARY_ACC_NO;
import static com.dms.util.CoreBankSmsReaderParam.STATUS;
import static com.dms.util.CoreBankSmsReaderParam.TXN_CATEGORY;
import static com.dms.util.CoreBankSmsReaderParam.TXN_DATE;
import static com.dms.util.CoreBankSmsReaderParam.TXN_TRACE_NO;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dms.service.SmsTransactionService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.dms.service.SmsTransactionDetailsService;

/**
 *
 * @author Lakshitha
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CoreBankSmsReaderImpl implements CoreBankSmsReader {

    final static Logger LOGGER = Logger.getLogger(CoreBankSmsReaderImpl.class);

    @Autowired
    private SmsTransactionService transactionService;

    @Autowired
    private SmsTransactionDetailsService transactionDetailService;
    private final AS400 as400;

    @Autowired
    public CoreBankSmsReaderImpl(AS400 as400) {
        this.as400 = as400;
    }

    @Override
    public SmsTransaction findNextPendingTransaction() {
        SmsTransaction transaction = new SmsTransaction();
        try {
//            if (true) {
//                Random r = new Random();
//                String tr = String.valueOf(r.nextInt(100000));
//                System.out.println("Scheduler 1 Starting");
//                transaction.setTransactionId(tr);
//                transaction.setCustomerNo("CUS001");
//                transaction.setMaskedAccountNo("AAA100");
//                transaction.setAmount(10000.00);
//                transaction.setTransactionCategory("CAT001");
//                transaction.setReferenceNo("REF001");
//                transaction.setChequeNo("CHQ001");
//                transaction.setSecondaryAccountNo("SEC001");
//                transaction.setDueDate("20141212");
//                transaction.setTxnTraceNo(1);
//                transaction.setTxnDatetime("20141212");
//                transaction.setStatus("T");
//                transaction.setEntPc("lakiya");
//                transaction.setModPc("LAKIYA");
//
//                System.out.println("Tran " + transaction.getTransactionId() );
//                List<TransactionDetail> transactionDetailList = new ArrayList<>();
//                String mobileNoListString = "0772250828|0231232333";
//                for (String mobileNo : mobileNoListString.split("\\|")) {
//                    TransactionDetail transactionDetail = new TransactionDetail();
//                    TransactionDetailPK primaryKey = new TransactionDetailPK();
//                    transactionDetail.setMobileNo(String.valueOf(r.nextInt(100000)));
//                    transactionDetail.setTransactionId(tr);
//                    transactionDetail.setEntPc("LAKIYA");
//                    transactionDetail.setModPc("LAKIYA");
//                    transactionDetail.setSmsStatus(SMSStatus.SAVED.getSmsStatusId());
//                    transactionDetail.setCbsStatus(SMSCoreStatus.PENDING.getSmsCoreStatusId());
//                    this.transactionDetailService.save(transactionDetail);
//                    transactionDetailList.add(transactionDetail);
//                }
//                this.transactionService.save(transaction);
//                return transaction;
//            }

            transaction = new SmsTransaction();
            ProgramParameter[] enquiryParameterList = new ProgramParameter[13];
            enquiryParameterList[MASKED_ACCOUNT_NO] = new ProgramParameter(20);
            enquiryParameterList[CUSTOMER_NO] = new ProgramParameter(6);
            enquiryParameterList[MOBILE_NO] = new ProgramParameter(65);
            enquiryParameterList[AMOUNT] = new ProgramParameter(13);
            enquiryParameterList[TXN_CATEGORY] = new ProgramParameter(6);
            enquiryParameterList[REF_NO] = new ProgramParameter(30);
            enquiryParameterList[CHEQUE_NO] = new ProgramParameter(16);
            enquiryParameterList[SECONDARY_ACC_NO] = new ProgramParameter(20);
            enquiryParameterList[DUE_DATE] = new ProgramParameter(8);
            enquiryParameterList[TXN_TRACE_NO] = new ProgramParameter(14);
            enquiryParameterList[TXN_DATE] = new ProgramParameter(19);
            enquiryParameterList[STATUS] = new ProgramParameter(1);
            enquiryParameterList[EXCEPTION] = new ProgramParameter(1);

            ProgramCall pgm = new ProgramCall(as400);
            pgm.setProgram("/QSYS.LIB/SMSLIB.LIB/UBSM0210.PGM", enquiryParameterList);
            if (!pgm.run()) {
                AS400Message[] messageList = pgm.getMessageList();
                for (AS400Message message : messageList) {
                    LOGGER.error("Result Error");
                    LOGGER.error(message.getID() + " : " + message.getText());
                }
            } else {
                byte[] bMaskedAccountNo = enquiryParameterList[MASKED_ACCOUNT_NO].getOutputData();
                byte[] bCustomerNo = enquiryParameterList[CUSTOMER_NO].getOutputData();
                byte[] bMobileNo = enquiryParameterList[MOBILE_NO].getOutputData();
                byte[] bAmount = enquiryParameterList[AMOUNT].getOutputData();
                byte[] bTxnCategory = enquiryParameterList[TXN_CATEGORY].getOutputData();
                byte[] breferenceNo = enquiryParameterList[REF_NO].getOutputData();
                byte[] bchequeNo = enquiryParameterList[CHEQUE_NO].getOutputData();
                byte[] bSecondaryAccountNo = enquiryParameterList[SECONDARY_ACC_NO].getOutputData();
                byte[] bDueDate = enquiryParameterList[DUE_DATE].getOutputData();
                byte[] bTxnTraceNo = enquiryParameterList[TXN_TRACE_NO].getOutputData();
                byte[] bTxnDate = enquiryParameterList[TXN_DATE].getOutputData();
                byte[] bStatus = enquiryParameterList[STATUS].getOutputData();
                byte[] bException = enquiryParameterList[EXCEPTION].getOutputData();

                AS400Text as400Text;

                as400Text = new AS400Text(20, as400);
                transaction.setMaskedAccountNo(as400Text.toObject(bMaskedAccountNo).toString());
                as400Text = new AS400Text(6, as400);
                transaction.setCustomerNo(as400Text.toObject(bCustomerNo).toString());
                as400Text = new AS400Text(65, as400);
                String mobileNoListString = as400Text.toObject(bMobileNo).toString();

                List<SmsTransactionDetail> transactionDetailList = new ArrayList<>();

                for (String mobileNo : mobileNoListString.split("|")) {
                    SmsTransactionDetail transactionDetail = new SmsTransactionDetail();
                    TransactionDetailPK primaryKey = new TransactionDetailPK();
                    primaryKey.setMobileNo(mobileNo);
                    transactionDetail.setSmsStatus(SMSStatus.SAVED.getSmsStatusId());
                    transactionDetail.setCbsStatus(SMSCoreStatus.PENDING.getSmsCoreStatusId());
                    transactionDetailList.add(transactionDetail);
                }
                transaction.setTransactionDetailList(transactionDetailList);

                AS400PackedDecimal as400AccountBalance = new AS400PackedDecimal(13, 2);
                transaction.setAmount(as400AccountBalance.toDouble(bAmount));

                as400Text = new AS400Text(6, as400);
                transaction.setTransactionCategory(as400Text.toObject(bTxnCategory).toString());
                as400Text = new AS400Text(30, as400);
                transaction.setReferenceNo(as400Text.toObject(breferenceNo).toString());
                as400Text = new AS400Text(16, as400);
                transaction.setChequeNo(as400Text.toObject(bchequeNo).toString());
                as400Text = new AS400Text(20, as400);
                transaction.setSecondaryAccountNo(as400Text.toObject(bSecondaryAccountNo).toString());
                as400Text = new AS400Text(8, as400);
                transaction.setDueDate(as400Text.toObject(bDueDate).toString());
                as400Text = new AS400Text(14, as400);
                transaction.setTxnTraceNo(Integer.parseInt(as400Text.toObject(bTxnTraceNo).toString()));
                as400Text = new AS400Text(14, as400);
                String strTxnDateTime = as400Text.toObject(bTxnDate).toString();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
                Date txnDateTime = sdf.parse(strTxnDateTime);
                transaction.setTxnDatetime(txnDateTime);
                as400Text = new AS400Text(1, as400);
                transaction.setStatus(as400Text.toObject(bStatus).toString());
                as400Text = new AS400Text(1, as400);
                transaction.setException(as400Text.toObject(bException).toString());
            }
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
        return transaction;
    }
}
