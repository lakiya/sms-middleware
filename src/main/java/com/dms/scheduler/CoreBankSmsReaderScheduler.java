/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.scheduler;

import com.dms.entity.SmsTransaction;
import com.dms.entity.SmsTransactionDetail;
import com.dms.enums.CBSTransactionStatus;
import com.dms.manager.CoreBankSmsReader;
import com.dms.managerImpl.CoreBankSmsReaderImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.dms.service.SmsTransactionService;
import java.util.List;
import com.dms.service.SmsTransactionDetailsService;

/**
 *
 * @author Lakshitha
 */
@Service
public class CoreBankSmsReaderScheduler {

    final static Logger LOGGER = Logger.getLogger(CoreBankSmsReaderScheduler.class);
    private final CoreBankSmsReader coreBankSmsReader;
    private final SmsTransactionService transactionService;
    private final SmsTransactionDetailsService transactionDetailService;

    @Autowired
    public CoreBankSmsReaderScheduler(CoreBankSmsReader coreBankSmsReader,
            SmsTransactionService transactionService,
            SmsTransactionDetailsService transactionDetailService) {
        this.coreBankSmsReader = coreBankSmsReader;
        this.transactionService = transactionService;
        this.transactionDetailService = transactionDetailService;
    }

    @Scheduled(fixedRate = 60000)
    public void processPendingTransactions() {
        boolean isNextTxnAvailable;
        try {
            do {            
                SmsTransaction transaction = this.coreBankSmsReader.findNextPendingTransaction();
                this.transactionService.save(transaction);
                List<SmsTransactionDetail> transactionDetailList = transaction.getTransactionDetailList();
                transactionDetailList.forEach(i -> i.setTransactionId(transaction.getTransactionId()));
                this.transactionDetailService.save(transactionDetailList);
                isNextTxnAvailable = transaction.getStatus().equals(CBSTransactionStatus.NEXT_TRANSACTION.getTransactionStatus());
                System.out.println("Is Available " + isNextTxnAvailable);
            } while (isNextTxnAvailable);
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }
}
