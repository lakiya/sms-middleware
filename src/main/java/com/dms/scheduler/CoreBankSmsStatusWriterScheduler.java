/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.scheduler;

import com.dms.entity.SmsTransactionDetail;
import com.dms.enums.SMSCoreStatus;
import com.dms.enums.SMSStatus;
import com.dms.manager.CoreBankSmsStatusWriter;
import com.dms.service.SmsTransactionDetailsService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lakshitha
 */
@Service
public class CoreBankSmsStatusWriterScheduler {

    final static Logger LOGGER = Logger.getLogger(CoreBankSmsStatusWriterScheduler.class);
    private final CoreBankSmsStatusWriter coreBankSmsStatusWriter;
    private final SmsTransactionDetailsService smsTransactionDetailService;

    @Autowired
    public CoreBankSmsStatusWriterScheduler(CoreBankSmsStatusWriter coreBankSmsStatusWriter,
            SmsTransactionDetailsService smsTransactionDetailService) {
        this.coreBankSmsStatusWriter = coreBankSmsStatusWriter;
        this.smsTransactionDetailService = smsTransactionDetailService;
    }

    @Scheduled(fixedRate = 60000)
    public void readFromDbAndUpdateCore() {
        try {
            List<SmsTransactionDetail> smsTransactionDetailsList = this.smsTransactionDetailService.findBySmsStatusNotAndCbsStatus(SMSStatus.SAVED.getSmsStatusId(), SMSCoreStatus.PENDING.getSmsCoreStatusId());
            for(SmsTransactionDetail smsTransactionDetail : smsTransactionDetailsList){
                boolean isSuccess = this.coreBankSmsStatusWriter.updateCoreBankTransactionDetailStatus(smsTransactionDetail);
                smsTransactionDetail.setCbsStatus(isSuccess ? SMSCoreStatus.CORE_UPDATE_SUCCESS.getSmsCoreStatusId() : SMSCoreStatus.CORE_UPDATE_FAILED.getSmsCoreStatusId());
                this.smsTransactionDetailService.save(smsTransactionDetail);
            }
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }
}
