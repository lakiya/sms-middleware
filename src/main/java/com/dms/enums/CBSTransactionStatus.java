/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.enums;

/**
 *
 * @author Lakshitha
 */
public enum CBSTransactionStatus {
    DEFAULT(0, "C"),
    NEXT_TRANSACTION(1, "T"),
    EXCEPTION(2,"E"),
    NO_TRANSACTION(3, "N");
    
    private final Integer transactionStatusId;
    private final String transactionStatus;

    CBSTransactionStatus(Integer transactionStatusId, String transactionStatus){
        this.transactionStatusId = transactionStatusId;
        this.transactionStatus = transactionStatus;
    }

    public Integer getTransactionStatusId() {
        return transactionStatusId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }
}
