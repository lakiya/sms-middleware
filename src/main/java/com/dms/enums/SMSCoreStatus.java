/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.enums;

/**
 *
 * @author Lakshitha
 */
public enum SMSCoreStatus {
    PENDING(1, "PENDING"),
    CORE_UPDATE_SUCCESS(2, "SUCCESS"),
    CORE_UPDATE_FAILED(3, "FAIL");
    
    private final Integer smsCoreStatusId;
    private final String smsCoreStatus;

    SMSCoreStatus(Integer smsCoreStatusId, String smsCoreStatus) {
        this.smsCoreStatusId = smsCoreStatusId;
        this.smsCoreStatus = smsCoreStatus;
    }

    public Integer getSmsCoreStatusId() {
        return smsCoreStatusId;
    }

    public String getSmsCoreStatus() {
        return smsCoreStatus;
    }    
}
