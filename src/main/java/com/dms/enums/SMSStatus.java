/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dms.enums;

/**
 *
 * @author Lakshitha
 */
public enum SMSStatus {
    SAVED(1, "SAVED"),
    SUCCESS(2, "SUCCSS"),
    FAIL(3, "FAIL");
    
    private final Integer smsStatusId;
    private final String smsStatus;

    SMSStatus(Integer smsStatusId, String smsStatus){
        this.smsStatusId = smsStatusId;
        this.smsStatus = smsStatus;
    }

    public Integer getSmsStatusId() {
        return smsStatusId;
    }

    public String getSmsStatus() {
        return smsStatus;
    }
}
